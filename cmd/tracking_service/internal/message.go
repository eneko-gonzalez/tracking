/*
 * Copyright (C) 2021 Orange & contributors
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 *
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */

package internal

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"
)

type TemplateDescription struct {
	Name    string `json:"name"`
	Version string `json:"version"`
}

type CiId int

type Timestamp string

func CurrentTimestamp() Timestamp {
	return Timestamp(strings.ReplaceAll(fmt.Sprintln(time.Now().UTC().Format("2006-01-02T15:04:05Z")), "\n", ""))
}

type CiUser struct {
	Id    CiId   `json:"id"`
	Login string `json:"login"`
}

type CiProject struct {
	Id   CiId   `json:"id"`
	Path string `json:"path"`
}

type CiBuild struct {
	CommitReference string `json:"ref"`
}

type CiJob struct {
	Id    CiId   `json:"id"`
	Name  string `json:"name"`
	Stage string `json:"stage"`
	Url   string `json:"url"`
}

type CiPipeline struct {
	Id  CiId   `json:"id"`
	Url string `json:"url"`
}

type CiRunner struct {
	Id          CiId   `json:"id"`
	Description string `json:"description"`
	Tags        string `json:"tags"`
	Version     string `json:"version"`
}

type CiInformation struct {
	User     CiUser     `json:"user"`
	Project  CiProject  `json:"project"`
	Build    CiBuild    `json:"build"`
	Job      CiJob      `json:"job"`
	Pipeline CiPipeline `json:"pipeline"`
	Runner   CiRunner   `json:"runner"`
}

type Message struct {
	MessageType         string              `json:"@type"`
	Timestamp           Timestamp           `json:"@timestamp"`
	TemplateDescription TemplateDescription `json:"template"`
	CiInformation       CiInformation       `json:"ci"`
}

func getCiIdFromEnv(variable string) CiId {
	if id, err := strconv.Atoi(os.Getenv(variable)); err != nil {
		return CiId(-1)
	} else {
		return CiId(id)
	}
}

func getStringFromEnv(variable string) string {
	return os.Getenv(variable)
}

func CiTrackingMessage(templateName string, version string) *Message {
	return &Message{
		MessageType: "ci-job",
		Timestamp:   CurrentTimestamp(),
		TemplateDescription: TemplateDescription{
			Name:    templateName,
			Version: version},
		CiInformation: CiInformation{
			User: CiUser{
				Id:    getCiIdFromEnv("GITLAB_USER_ID"),
				Login: getStringFromEnv("GITLAB_USER_LOGIN")},
			Project: CiProject{
				Id:   getCiIdFromEnv("CI_PROJECT_ID"),
				Path: getStringFromEnv("CI_PROJECT_PATH")},
			Build: CiBuild{
				CommitReference: getStringFromEnv("CI_COMMIT_REF_NAME")},
			Job: CiJob{
				Id:    getCiIdFromEnv("CI_JOB_ID"),
				Name:  getStringFromEnv("CI_JOB_NAME"),
				Stage: getStringFromEnv("CI_JOB_STAGE"),
				Url:   getStringFromEnv("CI_JOB_URL")},
			Pipeline: CiPipeline{
				Id:  getCiIdFromEnv("CI_PIPELINE_ID"),
				Url: getStringFromEnv("CI_PIPELINE_URL")},
			Runner: CiRunner{
				Id:          getCiIdFromEnv("CI_RUNNER_ID"),
				Description: getStringFromEnv("CI_RUNNER_DESCRIPTION"),
				Tags:        getStringFromEnv("CI_RUNNER_TAGS"),
				Version:     getStringFromEnv("CI_RUNNER_VERSION")},
		},
	}
}
